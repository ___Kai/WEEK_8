using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapTimeManager : MonoBehaviour
{
    public static int minuteCount;
    public static int secondsCount;
    public static float millisecondCount;
    public static string millisecondDisplay;
    public GameObject minuteBox;
    public GameObject secondsBox;
    public GameObject millisecondBox;

    // Update is called once per frame
    void Update()
    {
        millisecondCount += Time.deltaTime * 10;
        millisecondDisplay = millisecondCount.ToString("F0");
        millisecondBox.GetComponent<Text>().text = "" + millisecondDisplay;
        if(millisecondCount >= 10)
        {
            millisecondCount = 0;
            secondsCount += 1;
        }
        if(secondsCount <= 9)
        {
            secondsBox.GetComponent<Text>().text = "0" + secondsCount + ".";
        }
        else
        {
            secondsBox.GetComponent<Text>().text = "" + secondsCount + ".";
        }
        if(secondsCount >= 60)
        {
            secondsCount = 0;
            minuteCount += 1;
        }
        if(minuteCount <= 9)
        {
            minuteBox.GetComponent<Text>().text = "0" + minuteCount + ":";
        }
        else
        {
            minuteBox.GetComponent<Text>().text = "" + minuteCount + ":";
        }
    }
}
