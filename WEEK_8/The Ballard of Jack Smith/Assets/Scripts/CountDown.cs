using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    public GameObject countdown;
    public AudioSource bopAudio;
    public AudioSource ghengAudio;
    public GameObject lapTimer;
    public GameObject carControls;

    private void Start()
    {
        StartCoroutine(CountStart());
    }
    
    IEnumerator CountStart()
    {
        lapTimer.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        countdown.GetComponent<Text>().text = "3";
        bopAudio.Play();
        countdown.SetActive(true);
        yield return new WaitForSeconds(1);
        countdown.SetActive(false);
        countdown.GetComponent<Text>().text = "2";
        bopAudio.Play();
        countdown.SetActive(true);
        yield return new WaitForSeconds(1);
        countdown.SetActive(false);
        countdown.GetComponent<Text>().text = "1";
        bopAudio.Play();
        countdown.SetActive(true);
        yield return new WaitForSeconds(1);
        countdown.SetActive(false);
        ghengAudio.Play();
        lapTimer.SetActive(true);
        carControls.SetActive(true);
    }
}
