﻿using UnityEngine;

[AddComponentMenu("Actors/Patrol Waypoint")]
public class WayPoint : MonoBehaviour
{
    [HideInInspector]
    public WayPoint nextWaypoint;
    public float waitAtThisPoint = 0.0f;
    [HideInInspector]
    public Patrol patrol;




    private void OnDrawGizmos()
    {
        if (nextWaypoint)
        {
            Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 0.5f);
            Gizmos.DrawSphere(transform.position, 0.25f);
            Gizmos.DrawLine(transform.position, nextWaypoint.transform.position);
        }
        else
        {
            Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
            Gizmos.DrawSphere(transform.position, 0.25f);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (nextWaypoint)
        {
            Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
            Gizmos.DrawSphere(transform.position, 0.25f);
            Gizmos.DrawLine(transform.position, nextWaypoint.transform.position);
        }
        else
        {
            Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            Gizmos.DrawSphere(transform.position, 0.25f);
        }
    }

    [ContextMenu("Delete Waypoint")]
    public void RemoveWaypointFromPath()
    {
        patrol.RemoveWayPoint(this);
        DestroyImmediate(gameObject);
    }

    public Vector3 Position
    {
        get { return transform.position; }
    }
}
