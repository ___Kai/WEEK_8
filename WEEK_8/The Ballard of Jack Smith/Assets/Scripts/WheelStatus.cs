﻿using UnityEngine;

public class WheelStatus : MonoBehaviour
{
    protected WheelCollider linkedWheel;

    protected float resetWheelCountdown = -1f;
    protected WheelFrictionCurve forwardFriction;
    protected WheelFrictionCurve sidewaysFriction;

    // Use this for initialization
    void Start()
    {
        linkedWheel = GetComponent<WheelCollider>();

        // store the state of the wheel so we can restore it
        forwardFriction = linkedWheel.forwardFriction;
        sidewaysFriction = linkedWheel.sidewaysFriction;
    }

    // Update is called once per frame
    void Update()
    {
        // is a wheel effect active?
        if (resetWheelCountdown > 0)
        {
            // reduce the time remaining on the effect
            resetWheelCountdown -= Time.deltaTime;

            // is it time to reset the wheel to its defaults?
            if (resetWheelCountdown <= 0)
            {
                ResetWheel();
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        // check if we hit something that can effect the wheel, if so apply the effect
        EffectWheel effect = other.gameObject.GetComponent<EffectWheel>();
        if (effect != null)
        {
            ApplyEffect(effect);
        }
    }

    public void ApplyEffect(EffectWheel effect)
    {
        resetWheelCountdown = effect.EffectDuration;

        WheelFrictionCurve temporary = linkedWheel.sidewaysFriction;
        temporary.stiffness = 10;
        linkedWheel.sidewaysFriction = temporary;
    }

    public void ResetWheel()
    {
        linkedWheel.forwardFriction = forwardFriction;
        linkedWheel.sidewaysFriction = sidewaysFriction;
    }
}
