using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CarControllerActive : MonoBehaviour
{

    public GameObject CarControl;
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Enemy3;

    void Start()
    {
        CarControl.GetComponent<CarUserControl>().enabled = true;
        Enemy1.GetComponent<CarAIControl>().enabled = true;
        Enemy2.GetComponent<CarAIControl>().enabled = true;
        Enemy3.GetComponent<CarAIControl>().enabled = true;
    }
}
