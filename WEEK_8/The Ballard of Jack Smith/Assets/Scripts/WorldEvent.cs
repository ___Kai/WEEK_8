﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class WorldEvent : MonoBehaviour
{
    [Tooltip("The name of the event (used in Warning phase and to call event). ")]
    [SerializeField] protected string eventName;

    public float messageTime;
    public List<string> warningMessages;

    [Tooltip("The length of the event's active phase in seconds.")]
    [SerializeField] protected float activeDuration;
    public float ActiveDuration => activeDuration;

    [Tooltip("The length of the event's cooldown phase in seconds.")]
    public float cooldownDuration;

    [Tooltip("The length of the event's warning phase in seconds.")]
    public float warningDuration;

    protected float eventTimer;

    public AudioSource eventAudioSource;
    protected AudioSource backgroundMusic;
    protected float originalMusicVolume;
    public float soundEffectVolume;
    public float muffledMusicVolume;
    public bool isLoopingSoundEffect;
    public bool isBackgroundMusicMuffled;
    public GameObject particleEffectPrefab;
    protected ParticleSystem particleEffects;
    
    public enum State { Inactive, Warning, Active, Cooldown };
    protected State currentState;
    public State CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            if (currentState == value)
                return;

            // If state goes from inactive to anything else, call OnWarningPhaseStart
            if (currentState == State.Inactive && value != State.Inactive)
            {
                OnWarningPhaseStart();
            }

            // If state goes from anything to inactive, call OnEventFinish
            if (currentState != State.Inactive && value == State.Inactive)
            {
                OnCooldownPhaseFinish();
            }

            // If state goes from anything to active, call OnActiveStart
            if (currentState != State.Active && value == State.Active)
            {
                UnityEventManager.OnEnemySpawn += OnEnemySpawn;
                OnActiveStart();
            }

            // If state goes from active to anything else, call OnActiveFinish
            if (currentState == State.Active && value != State.Active)
            {
                UnityEventManager.OnEnemySpawn -= OnEnemySpawn;
                OnActiveFinish();
            }

            currentState = value;
            UnityEventManager.OnWorldEventChange?.Invoke(this);
        }
    }

    public string EventName => eventName;
    public float TotalDuration => warningDuration + activeDuration + cooldownDuration;

    // Runs every time an enemy spawns
    public virtual void OnEnemySpawn(GameObject enemy) { }
    
    // Runs once at the start of the warning phase
    public virtual void OnWarningPhaseStart()
    {
        //TODO: Ensure that messages are not repeating.
        if (warningMessages.Count > 0)
        {
            UnityEventManager.SendMessage?.Invoke(warningMessages[Random.Range(0, warningMessages.Count)], messageTime);
        }

        if (isLoopingSoundEffect && eventAudioSource != null)
        {
            eventAudioSource.loop = true;
            eventAudioSource.Play();
        }

        if (particleEffects != null)
        {
            particleEffects.Play();
        }
    }
    
    // Runs once at the start of the active phase
    public virtual void OnActiveStart()
    {
        if (!isLoopingSoundEffect && eventAudioSource != null)
        {
            eventAudioSource.volume = soundEffectVolume;
            eventAudioSource.loop = false;
            eventAudioSource.Play();
        }
    }

    // Runs once at the end of the active phase
    public virtual void OnActiveFinish()
    {
        if (!isLoopingSoundEffect && eventAudioSource != null)
        {
            eventAudioSource.Stop();
        }
    }

    // Runs once at the end of the cooldown phase
    public virtual void OnCooldownPhaseFinish()
    {
        if (eventAudioSource != null)
        {
            eventAudioSource.Stop();
        }

        if (particleEffects != null)
        {
            particleEffects.Stop();
        }
    }

    // Runs every frame while event is in warning state
    public virtual void UpdateWarningEvent()
    {
        if (isLoopingSoundEffect)
        {
            float lerpValue = eventTimer / warningDuration;
            if (eventAudioSource != null)
            {
                eventAudioSource.volume = Mathf.Lerp(0, soundEffectVolume, lerpValue);
            }
            if (backgroundMusic != null && isBackgroundMusicMuffled)
            {
                backgroundMusic.volume = Mathf.Lerp(originalMusicVolume, muffledMusicVolume, lerpValue);
            }
        }        
    }

    // Runs every frame while event is in active state
    public virtual void UpdateActiveEvent() { }

    // Runs every frame while event is in cooldown state
    public virtual void UpdateCoolDownEvent()
    {
        if (isLoopingSoundEffect)
        {
            float lerpValue = eventTimer / warningDuration;
            if (eventAudioSource != null)
            {
                eventAudioSource.volume = Mathf.Lerp(soundEffectVolume, 0, lerpValue);
            }
            if (backgroundMusic != null && isBackgroundMusicMuffled)
            {
                backgroundMusic.volume = Mathf.Lerp(muffledMusicVolume, originalMusicVolume, lerpValue);
            }
        }
    }

    // Continues warning coroutine until warning duration is up
    protected virtual void ProcessWarningState()
    {
        if (eventTimer >= warningDuration)
        {
            eventTimer -= warningDuration;
            CurrentState = State.Active;
        }
        else
        {
            UpdateWarningEvent();
        }
    }
    
    // Updates active event until active duration is up
    protected virtual void ProcessActiveState()
    {
        if (eventTimer >= activeDuration)
        {
            eventTimer -= activeDuration;
            CurrentState = State.Cooldown;
        }
        else
        {
            UpdateActiveEvent();
        }
    }

    // Waits until cooldown is over
    protected virtual void ProcessCoolDown()
    {
        if (eventTimer >= cooldownDuration)
        {
            eventTimer = 0;
            CurrentState = State.Inactive;
        }
        else
        {
            UpdateCoolDownEvent();
        }
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        backgroundMusic = GameObject.FindGameObjectWithTag("BootStrap").GetComponent<AudioSource>();
        if (backgroundMusic != null)
        {
            originalMusicVolume = backgroundMusic.volume;
        }
        if (eventAudioSource == null)
        {
            eventAudioSource = gameObject.GetComponent<AudioSource>();
        }
        eventTimer = 0;
        currentState = State.Inactive;
        if (particleEffectPrefab != null)
        {
            particleEffects = particleEffectPrefab.GetComponent<ParticleSystem>();
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (CurrentState == State.Inactive)
            return;

        eventTimer += Time.deltaTime;

        switch (CurrentState)
        {
            case State.Inactive:
                break;

            case State.Warning:
                ProcessWarningState();
                break;

            case State.Active:
                ProcessActiveState();
                break;

            case State.Cooldown:
                ProcessCoolDown();
                break;

            default:
            Debug.LogError($"{CurrentState} is unhandled");
            break;
        }
    }
}
