using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfLapTrigger : MonoBehaviour
{
    public GameObject lapCompletedTrigger;
    public GameObject halfLapTrigger;

    private void OnTriggerEnter()
    {
        lapCompletedTrigger.SetActive(true);
        halfLapTrigger.SetActive(false);
    }
}
