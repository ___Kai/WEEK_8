﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityEventManager
{
    public delegate void VoidDelegate();
    public static VoidDelegate OnPlayerFire;
    
    public delegate void EventDelegate(WorldEvent worldEvent);
    public static EventDelegate OnWorldEventChange;

    public delegate void VoiceLineDelegate(string message, float duration);
    public static VoiceLineDelegate SendMessage;

    public delegate void DescriptionDelegate(string description);
    public static DescriptionDelegate DisplayDesc;

    public delegate void GameObjectDelegate(GameObject Object);
    public static GameObjectDelegate OnEnemySpawn;
    public static GameObjectDelegate OnObjectDeath;

    public delegate void CameraShakeDelegate(float Intensity, float Duration);
    public static CameraShakeDelegate StartCameraShake;
}
