using UnityEngine;

[CreateAssetMenu]
[SerializeField]
public class AISpeedAdjustor : ScriptableObject
{
    [Range(0, 180)]
    public float decreaseTorqueAngle;
    [Range(0, 1)]
    public float torgueDecreaseRate;
    public float handBrakeTimer;
    [Range(0, 180)]
    public float brakingDist;
    [Range(0, 100)]
    public float brakingRate;
    [Range(0, 5)]
    public int pointsToLookAhead;
    public bool hasUsedHandBreak;
}
