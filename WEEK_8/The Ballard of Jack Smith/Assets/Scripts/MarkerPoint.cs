﻿using UnityEngine;

public class MarkerPoint : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        // turn off all children
        for (int index = 0; index < transform.childCount; ++index)
        {
            transform.GetChild(index).gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
