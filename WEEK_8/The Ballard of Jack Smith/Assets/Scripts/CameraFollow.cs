﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    #region Variables

    [Tooltip("The object to follow.")]
    public Transform objectToFollowTransform;

    private Rigidbody objectToFollowRigidbody; // The rigidbody of the object to follow (if it has one)

    private const float cameraConstant = -10f; // How 'zoomed out' the camera is, doesn't change view

    [Tooltip("The distance in front of the object the camera is.")]
    public float offsetDistance = 0.7f;

    [Tooltip("How much the velocity of the object affects the camera.")]
    public float velocityModifier = 0.08f;
    
    [Tooltip("The distance away from the edge of the screen the object is clamped to.")]
    public float cameraEdgeBuffer = 1f;

    [Tooltip("Whether to start or stop the camera shake.")]
    [SerializeField] protected bool isCameraShaking = false;

    [SerializeField]
    protected float baseSmoothDampTime = 1;
    private Vector3 cameraVelocity;

    private Vector3 originalPosition;

    public bool IsCameraShaking
    {
        get
        {
            return isCameraShaking;
        }
        set
        {
            shakeTimer = 0f;
            isCameraShaking = value;
        }
    }

    [Tooltip("How intensely the camera shakes.")]
    [Range(0f, 5f)] public float shakeIntensity = 1f;

    [Tooltip("How long the camera shakes.")]
    [Range(0f, 10f)] public float shakeDuration = 1f;

    private float shakeTimer = 0f;

    #endregion Variables

    private void Start()
    {
        // If object to follow has a 2D rigidbody, get a reference to it.
        if (objectToFollowTransform.GetComponent<Rigidbody>())
        {
            objectToFollowRigidbody = objectToFollowTransform.GetComponent<Rigidbody>();
        }

        UnityEventManager.StartCameraShake += StartCameraShake;
    }

    private void Update()
    {
        // Calculate and move camera to new position.
        FollowObject();
    }

    /// <summary>
    /// Moves the camera based on the object's velocity and direction.
    /// </summary>
    private void FollowObject()
    {
        if (objectToFollowTransform != null)
        {
            // Set the base camera position on the object.
            Vector3 newCameraPosition = new Vector3(objectToFollowTransform.position.x, objectToFollowTransform.position.y, cameraConstant);

            // Offset the camera in the direction the object is facing.
            newCameraPosition += objectToFollowTransform.up * offsetDistance;

            if (objectToFollowRigidbody != null)
            {
                // Add percentage of object's current velocity to camera position.
                newCameraPosition += ((Vector3)objectToFollowRigidbody.velocity) * velocityModifier;
            }


            // Find the screen width and height for bounding using top left corner
            float cameraHeight = Camera.main.orthographicSize;
            float cameraWidth = cameraHeight * (Screen.width / Screen.height);

            // Set bounds of camera in rectangular pattern
            newCameraPosition = new Vector3(Mathf.Clamp(newCameraPosition.x, objectToFollowTransform.position.x - cameraWidth + cameraEdgeBuffer, objectToFollowTransform.position.x + cameraWidth - cameraEdgeBuffer),
                Mathf.Clamp(newCameraPosition.y, objectToFollowTransform.position.y - cameraHeight + cameraEdgeBuffer, objectToFollowTransform.position.y + cameraHeight - cameraEdgeBuffer), cameraConstant);


            //transform.position = newCameraPosition;

            // Can use smoothDamp but makes the cameraShake suck.
            transform.position = Vector3.SmoothDamp(transform.position, newCameraPosition, ref cameraVelocity, baseSmoothDampTime);

            originalPosition = objectToFollowTransform.position;
            originalPosition.z = cameraConstant;

        }
        
        if (IsCameraShaking)
        {
            transform.position = originalPosition + CameraShake(shakeIntensity, shakeDuration);
        }
    }

    /// <summary>
    /// Returns a Vector3 with random values based on shake intensity.
    /// </summary>
    /// <param name="intensity"></param>
    /// <returns></returns>
    private Vector3 CameraShake(float intensity, float duration)
    {
        // Lower shake intensity based on elapsed time.
        float adjustedShakeIntensity = (1 - (shakeTimer / duration)) * intensity;

        //FROM STEVE: very cool, but you can do better than random, have a look at using Mathf.perlin
        //  if you want the next step from there, switch to using springs.

        // Add random shake to camera.
        Vector3 outputVector = new Vector3(Random.Range(-adjustedShakeIntensity, adjustedShakeIntensity),
            Random.Range(-adjustedShakeIntensity, adjustedShakeIntensity));

        // Adjust time and reset timer if shake is over.
        shakeTimer += Time.deltaTime;
        if (shakeTimer >= duration)
        {
            shakeTimer = 0;
            isCameraShaking = false;
        }

        return outputVector;
    }

    /// <summary>
    /// Starts the camera shake.
    /// </summary>
    /// <param name="intensity"></param>
    /// <param name="duration"></param>
    private void StartCameraShake(float intensity, float duration)
    {
        if (intensity > 0 && duration > 0)
        {
            shakeIntensity = intensity;
            shakeDuration = duration;
            isCameraShaking = true;
        }
    }
}
